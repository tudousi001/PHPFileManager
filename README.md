﻿## Synopsis

A single-file featured PHP file manager.

## Features

* Single file
* Users and permissions
* Save users in self or in SQLite database
* Upload a ZIP and extract
* Download files in ZIP
* jQuery, BootStrap, DataTables interface
* Login with form (https) or with HTTP Digest (http)

## Installation

Copy phpfm.php to your root directory and login as "root" without password.

## License

CPOL